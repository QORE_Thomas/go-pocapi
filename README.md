### How to build it
#### Dependencies
- github.com/go-swagger/go-swagger
- github.com/go-openapi/loads
- github.com/go-openapi/runtime
#### Generate the code using the definition file
`swagger generate server -t gen -f def.yaml --exclude-main -A <name>`
#### Code
Write your handlers in handlers/ using the ping example then add them in the main.go file.

You should document your code (godoc)

**Use** go-imports as a file watcher to format your code.

#### Build
`go build main.go`