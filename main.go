package main

import (
	"Ideka/pocapi/gen/restapi"
	"Ideka/pocapi/gen/restapi/operations"
	"Ideka/pocapi/handlers"
	"log"

	"github.com/go-openapi/loads"
)

func main() {
	spec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatal("main: couln't load swaggerJson")
	}
	api := operations.NewIdekaAPI(spec)
	server := restapi.NewServer(api)
	defer func() {
		err := server.Shutdown()

		if err != nil {
			log.Fatal("main: unable to shutdown the server: ")
		}
	}()
	server.Port = 8080
	api.GetPingHandler = handlers.NewPing()
	api.PostUserLoginHandler = handlers.NewLogin()
	api.PostUserRegisterHandler = handlers.NewRegister()

	handler := api.Serve(nil)
	server.SetHandler(handler)
	if err := server.Serve(); err != nil {
		log.Fatal("main: an server error occurred: ", err)
	}
}
