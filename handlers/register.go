package handlers

import (
	"Ideka/pocapi/gen/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

type register struct {
}

func (p *register) Handle(params operations.PostUserRegisterParams) middleware.Responder {
	return operations.NewPostUserRegisterOK()
}

func NewRegister() operations.PostUserRegisterHandler {
	return &register{}
}
