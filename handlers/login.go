package handlers

import (
	"Ideka/pocapi/gen/restapi/operations"

	"github.com/go-openapi/runtime/middleware"
)

type login struct {
}

func (p *login) Handle(params operations.PostUserLoginParams) middleware.Responder {
	return operations.NewPostUserLoginOK()
}

func NewLogin() operations.PostUserLoginHandler {
	return &login{}
}
