package handlers

import (
	"Ideka/pocapi/gen/models"
	"Ideka/pocapi/gen/restapi/operations"
	"time"

	"github.com/go-openapi/runtime/middleware"
)

type ping struct {
}

func (p *ping) Handle(params operations.GetPingParams) middleware.Responder {
	response := models.Ping{
		Message:   "Hey ! Wassup ?",
		Timestamp: time.Now().Unix(),
	}
	return operations.NewGetPingOK().WithPayload(&response)
}

func NewPing() operations.GetPingHandler {
	return &ping{}
}
